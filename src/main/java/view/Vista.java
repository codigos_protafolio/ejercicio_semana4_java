package view;


import java.sql.SQLException;
import java.util.ArrayList;

import controller.Controlador;
import model.vo.Lider;
import model.vo.Proyecto;

public class Vista {

    public static final Controlador controlador = new Controlador();

    public static void vista_requerimiento_1() {

        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_1();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Constructora: "+proyectos.get(i).getNombre_constructora()+" - Serial: "+proyectos.get(i).getSerial());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_2() {
        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_2();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Numero_Habitaciones: "+proyectos.get(i).getNum_habitaciones()+" - Numero_Banos: "+proyectos.get(i).getNum_banios()+" - Nombre_Lider: "+proyectos.get(i).getLider().getNombre()+" - Apellido_Lider: "+proyectos.get(i).getLider().getApellido()+" - Estrato_Proyecto: "+proyectos.get(i).getEstrato_proyecto());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_3() {
        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_3();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Cantidad_Casas: "+proyectos.get(i).getNum_casas()+" - Constructora: "+proyectos.get(i).getNombre_constructora());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_4() {
        try {

            ArrayList<Lider> lideres;
            lideres = controlador.Solucionar_requerimiento_4();
            for (int i = 0; i < lideres.size(); i++) {
                System.out.println("Nombre_Lider: "+lideres.get(i).getNombre()+" - Apellido_Lider: "+lideres.get(i).getApellido());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_5() {
        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_5();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Cantidad_Casas: "+proyectos.get(i).getNum_casas()+" - Constructora: "+proyectos.get(i).getNombre_constructora());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

}
