//Estructura de datos
import java.util.ArrayList;

//Librerías para SQL y Base de Datos
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.io.File;//Para verificación de longitud de base de datos

import java.sql.DriverManager;

class Lider{
    /*************
     * Atributos
     *************/
    private String nombre;
    private String apellido;

    //Constructor
    public Lider(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    //Consultores y Modificadores
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
   
}

class Proyecto {

    private String fecha_inicio;
    private int num_habitaciones;
    private int num_banios;
    private String nombre_constructora;
    private int estrato_proyecto;
    private String serial;
    private int num_casas;
    private String nombre_lider;
    private String apellido_lider;

    public Proyecto() {

    }

    public Proyecto(String fecha_inicio, int num_habitaciones, int num_banios, String nombre_constructora, int estrato_proyecto, String serial, int num_casas, Lider lider) {
        this.fecha_inicio = fecha_inicio;
        this.num_habitaciones = num_habitaciones;
        this.num_banios = num_banios;
        this.nombre_constructora = nombre_constructora;
        this.estrato_proyecto = estrato_proyecto;
        this.serial = serial;
        this.num_casas = num_casas;
        //this.lider = lider;

    }

    /*******************************
     * Consultores y modificadores
     ********************************/

    public String getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(String fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public int getNum_habitaciones() {
        return num_habitaciones;
    }

    public void setNum_habitaciones(int num_habitaciones) {
        this.num_habitaciones = num_habitaciones;
    }

    public int getNum_banios() {
        return num_banios;
    }

    public void setNum_banios(int num_banios) {
        this.num_banios = num_banios;
    }

    public String getNombre_constructora() {
        return nombre_constructora;
    }

    public void setNombre_constructora(String nombre_constructora) {
        this.nombre_constructora = nombre_constructora;
    }

    public String getNombreLider() {
        return nombre_lider;
    }

    public void setNombreLider(String nombre) {
        this.nombre_lider = nombre;
    }

    public String getApellidoLider() {
        return apellido_lider;
    }

    public void setApellidoLider(String apellido) {
        this.apellido_lider = apellido;
    }

    public int getEstrato_proyecto() {
        return estrato_proyecto;
    }

    public void setEstrato_proyecto(int estrato_proyecto) {
        this.estrato_proyecto = estrato_proyecto;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getNum_casas() {
        return num_casas;
    }

    public void setNum_casas(int num_casas) {
        this.num_casas = num_casas;
    }

    
}

class LiderDao {

    public ArrayList<Lider> query_requerimiento_4() throws SQLException {
        ArrayList<Lider> arrayLideres;
        arrayLideres = new ArrayList<Lider>();
        if (!JDBCUtilities.estaVacia()) {
            String query = "SELECT L.Nombre, L.Primer_Apellido ";
            query += "FROM Proyecto P ";
            query += "INNER JOIN Lider L ON L.ID_lider = P.ID_Lider ";
            query += "WHERE P.Clasificacion = 'Casa'";
            Connection conn = JDBCUtilities.getConnection();
            PreparedStatement pStatement = conn.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            while(resultado.next()){
                Lider objLider = new Lider(resultado.getString("Nombre"), resultado.getString("Primer_Apellido"));
                objLider.setNombre(resultado.getString("Nombre"));
                objLider.setApellido(resultado.getString("Primer_Apellido"));
                arrayLideres.add(objLider);
            }
            conn.close();
            pStatement.close();
        }else{
            System.out.println("¡No hay archivo de base de datos!");
        }
        return arrayLideres;
        
        
        /*
        Connection conexion = JDBCUtilities.getConnection();
        // Crea arreglo para almacenar objetos tipo Proyecto
        ArrayList<Lider> lideres = new ArrayList<Lider>();
        // Consultas

        return lideres;
        */
    }// Fin del método query_requerimiento_4

}

class ProyectoDao {

    public ArrayList<Proyecto> query_requerimiento_1() throws SQLException {
        ArrayList<Proyecto> arrayProyecto;
        arrayProyecto = new ArrayList<Proyecto>();
        if (!JDBCUtilities.estaVacia()) {
            String query = "SELECT Constructora, Serial ";
            query += "FROM Proyecto ";
            query += "WHERE Clasificacion = 'Casa'";
            Connection conn = JDBCUtilities.getConnection();
            PreparedStatement pStatement = conn.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            while(resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNombre_constructora( resultado.getString("Constructora"));
                objProyecto.setSerial(resultado.getString("Serial"));
                arrayProyecto.add(objProyecto);
            }   
            //Cerrar conexión
            conn.close();
            pStatement.close();
        }else{
            System.out.println("¡No hay archivo de base de datos!");
        }
        
        return arrayProyecto;
        
    }

    public ArrayList<Proyecto> query_requerimiento_2() throws SQLException {
        ArrayList<Proyecto> arrayProyecto;
        arrayProyecto = new ArrayList<Proyecto>();
        if (!JDBCUtilities.estaVacia()) {
            String query = "SELECT P.Numero_Habitaciones, P.Numero_Banos, L.Nombre, L.Primer_Apellido, T.Estrato ";
            query += "FROM Proyecto P ";
            query += "INNER JOIN Lider L ON L.ID_lider = P.ID_Lider ";
            query += "INNER JOIN Tipo T ON T.ID_Tipo = P.ID_Tipo ";
            query += "WHERE P.Clasificacion = 'Casa'";
            Connection conn = JDBCUtilities.getConnection();
            PreparedStatement pStatement = conn.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            while(resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_habitaciones( resultado.getInt("Numero_Habitaciones"));
                objProyecto.setNum_banios(resultado.getInt("Numero_Banos"));
                objProyecto.setEstrato_proyecto( resultado.getInt("Estrato"));
                objProyecto.setNombreLider(resultado.getString("Nombre"));
                objProyecto.setApellidoLider(resultado.getString("Primer_Apellido"));
                arrayProyecto.add(objProyecto);
            }
            conn.close();
            pStatement.close();
        }else{
            System.out.println("¡No hay archivo de base de datos!");
        }
        return arrayProyecto;
    }// Fin del método query_requerimiento_2


    public ArrayList<Proyecto> query_requerimiento_3() throws SQLException {
        ArrayList<Proyecto> arrayProyecto;
        arrayProyecto = new ArrayList<Proyecto>();
        if (!JDBCUtilities.estaVacia()) {
            String query = "SELECT count(Constructora) as Casas, Constructora ";
            query += "FROM Proyecto ";
            query += "WHERE Clasificacion = 'Casa' ";
            query += "GROUP BY Constructora";
            Connection conn = JDBCUtilities.getConnection();
            PreparedStatement pStatement = conn.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            while(resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_casas( resultado.getInt("Casas"));
                objProyecto.setNombre_constructora(resultado.getString("Constructora"));
                arrayProyecto.add(objProyecto);
            }
            conn.close();
            pStatement.close();
        }else{
            System.out.println("¡No hay archivo de base de datos!");
        }
        return arrayProyecto;
    }// Fin del método query_requerimiento_3


    public ArrayList<Proyecto> query_requerimiento_5() throws SQLException{
        ArrayList<Proyecto> arrayProyecto;
        arrayProyecto = new ArrayList<Proyecto>();
        if (!JDBCUtilities.estaVacia()) {
            String query = "SELECT count(Constructora) as Casas, Constructora ";
            query += "FROM Proyecto ";
            query += "WHERE Clasificacion = 'Casa' ";
            query += "GROUP BY Constructora ";
            query += "HAVING Casas >= 18 ";
            query += "ORDER BY Casas ASC";
            Connection conn = JDBCUtilities.getConnection();
            PreparedStatement pStatement = conn.prepareStatement(query);
            ResultSet resultado = pStatement.executeQuery();
            while(resultado.next()){
                Proyecto objProyecto = new Proyecto();
                objProyecto.setNum_casas( resultado.getInt("Casas"));
                objProyecto.setNombre_constructora(resultado.getString("Constructora"));
                arrayProyecto.add(objProyecto);
            }
            conn.close();
            pStatement.close();
        }else{
            System.out.println("¡No hay archivo de base de datos!");
        }
        return arrayProyecto;
    }// Fin del método query_requerimiento_4

}

class JDBCUtilities {

    // Atributos de clase para gestión de conexión con la base de datos
    private static final String UBICACION_BD = "ProyectosConstruccion.db";

    public static Connection getConnection() throws SQLException {
        String url = "jdbc:sqlite:" + UBICACION_BD;
        return DriverManager.getConnection(url);
    }

    public static boolean estaVacia() {
        File archivo = new File(JDBCUtilities.UBICACION_BD);
        return archivo.length() == 0;
    }

}

class Controlador {

    private final ProyectoDao proyectoDao;
    private final LiderDao liderDao;

    public Controlador() {
        this.proyectoDao = new ProyectoDao();
        this.liderDao = new LiderDao();
    }


    public ArrayList<Proyecto> Solucionar_requerimiento_1() throws SQLException {
        return this.proyectoDao.query_requerimiento_1();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_2() throws SQLException {
        return this.proyectoDao.query_requerimiento_2();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_3() throws SQLException {
        return this.proyectoDao.query_requerimiento_3();
    }

    public ArrayList<Lider> Solucionar_requerimiento_4() throws SQLException {
        return this.liderDao.query_requerimiento_4();
    }

    public ArrayList<Proyecto> Solucionar_requerimiento_5() throws SQLException {
        return this.proyectoDao.query_requerimiento_5();
    }

}

class Vista {

    public static final Controlador controlador = new Controlador();

    public static void vista_requerimiento_1() {

        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_1();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Constructora: "+proyectos.get(i).getNombre_constructora()+" - Serial: "+proyectos.get(i).getSerial());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }

    }

    public static void vista_requerimiento_2() {
        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_2();
            for (int i = 0; i < 50; i++) {
                System.out.println("Numero_Habitaciones: "+proyectos.get(i).getNum_habitaciones()+" - Numero_Banos: "+proyectos.get(i).getNum_banios()+" - Nombre_Lider: "+proyectos.get(i).getNombreLider()+" - Apellido_Lider: "+proyectos.get(i).getApellidoLider()+" - Estrato_Proyecto: "+proyectos.get(i).getEstrato_proyecto());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_3() {
        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_3();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Cantidad_Casas: "+proyectos.get(i).getNum_casas()+" - Constructora: "+proyectos.get(i).getNombre_constructora());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_4() {
        try {

            ArrayList<Lider> lideres;
            lideres = controlador.Solucionar_requerimiento_4();
            for (int i = 0; i < lideres.size(); i++) {
                System.out.println("Nombre_Lider: "+lideres.get(i).getNombre()+" - Apellido_Lider: "+lideres.get(i).getApellido());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

    public static void vista_requerimiento_5() {
        try {

            ArrayList<Proyecto> proyectos;
            proyectos = controlador.Solucionar_requerimiento_5();
            for (int i = 0; i < proyectos.size(); i++) {
                System.out.println("Cantidad_Casas: "+proyectos.get(i).getNum_casas()+" - Constructora: "+proyectos.get(i).getNombre_constructora());
            }

        } catch (SQLException e) {
            System.err.println("Ha ocurrido un error!" + e.getMessage());
        }
    }

}











public class PruebaSubir {

    public static void main(String[] args) {

        // Casos de prueba
    
        // Requerimiento 1 - Reto3
        //Vista.vista_requerimiento_1();
        Vista.vista_requerimiento_2();
        //Vista.vista_requerimiento_3();
        //Vista.vista_requerimiento_4();
        //Vista.vista_requerimiento_5();
    
    
    }
    
}

